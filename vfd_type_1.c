/**
 * @file vfd_type_1.c
 * @author Chris Owens (cowens@eemn.io)
 * @brief
 * @version 0.1
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022 Embedded Design Solutions, LLC.  All Rights Reserved.
 *
 */

/*******************************************************************************
 * Includes
 *******************************************************************************/
#include <stdio.h>
#include "vfd_type_1.h"

/*******************************************************************************
 * Module Macros
 *******************************************************************************/

/*******************************************************************************
 * Module Typedefs
 *******************************************************************************/

/*******************************************************************************
 * Module Variable Definitions
 *******************************************************************************/
static uint8_t get_speed(VFD_DRIVER* super);
static void set_speed(VFD_DRIVER* super, uint8_t speed);

static VFD_DRIVER_INTERFACE_STRUCT interface = {get_speed, set_speed};

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

/*******************************************************************************
 * Public Function Definitions
 *******************************************************************************/
void vfd_type_1_init(VFD_TYPE_1_STRUCT* self) {
  self->base.vtable = &interface;
  self->base.type = VFD_TYPE_1;
}

/*******************************************************************************
 * Private Function Definitions
 *******************************************************************************/
static uint8_t get_speed(VFD_DRIVER* super) {
  VFD_TYPE_1_STRUCT* self = (VFD_TYPE_1_STRUCT*)super;
  return self->speed;
}

static void set_speed(VFD_DRIVER* super, uint8_t speed) {
  VFD_TYPE_1_STRUCT* self = (VFD_TYPE_1_STRUCT*)super;
  self->speed = speed;
}
