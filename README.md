# OOP In C Example

A simple project to demonstrate how to use interfaces in c

## Building/Running

Ensure [make](https://pakstech.com/blog/make-windows/) is installed.


```bash
# to build
make

# to run
./main.exe
```
