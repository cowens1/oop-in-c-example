/**
 * @file main.c
 * @author Chris Owens (cowens@eemn.io)
 * @brief
 * @version 0.1
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022 Embedded Design Solutions, LLC.  All Rights Reserved.
 *
 */

/*******************************************************************************
 * Includes
 *******************************************************************************/
#include <stdio.h>
#include "vfd_driver.h"
#include "vfd_type_1.h"
#include "vfd_type_2.h"

/*******************************************************************************
 * Module Macros
 *******************************************************************************/

/*******************************************************************************
 * Module Typedefs
 *******************************************************************************/

/*******************************************************************************
 * Module Variable Definitions
 *******************************************************************************/
static VFD_TYPE_1_STRUCT vfdType1;
static VFD_TYPE_2_STRUCT vfdType2;

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

/*******************************************************************************
 * Public Function Definitions
 *******************************************************************************/
int main(void) {
  printf("Interface Examples\r\n");

  vfd_type_1_init(&vfdType1);
  vfd_type_2_init(&vfdType2);

  VFD_DRIVER* vfdWeAreUsing = (VFD_DRIVER*)&vfdType1;


  uint8_t speed = vfd_driver_get_speed(vfdWeAreUsing);
  printf("\r\nTrying vfd1 implementation\r\n");
  printf("vfd1 speed = %d\r\n", speed);
  printf("setting vfd_1_type speed to %d\r\n", 88);
  vfd_driver_set_speed(vfdWeAreUsing, 88);
  speed = vfd_driver_get_speed(vfdWeAreUsing);
  printf("vfd1 speed = %d\r\n", speed);

  printf("\r\nNow trying vfd2 implementation\r\n");
  vfdWeAreUsing = (VFD_DRIVER*)&vfdType2;

  speed = vfd_driver_get_speed(vfdWeAreUsing);
  printf("vfd2 speed = %d\r\n", speed);
  printf("setting vfd_2_type speed to %d\r\n", 88);
  vfd_driver_set_speed(vfdWeAreUsing, 88);
  speed = vfd_driver_get_speed(vfdWeAreUsing);
  printf("vfd2 speed = %d\r\n", speed);
  printf("notice how the returned vfd2 speed is double what it was set to.  This shows different interface implementations but still calling the same top level function\r\n");
}
/*******************************************************************************
 * Private Function Definitions
 *******************************************************************************/
