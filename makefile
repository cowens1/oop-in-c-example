CC=gcc
CFLAGS=-I. -g -O0
OBJ=main.o vfd_driver.o vfd_type_1.o vfd_type_2.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -rf *.o
