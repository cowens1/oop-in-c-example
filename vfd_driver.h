/**
 * @file vfd_driver.h
 * @author Chris Owens (cowens@eemn.io)
 * @brief
 * @version 0.1
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022 Embedded Design Solutions, LLC.  All Rights Reserved.
 *
 */

#ifndef VFD_DRIVER_H_
#define VFD_DRIVER_H_

/*******************************************************************************
 * Includes
 *******************************************************************************/
#include <stdint.h>

/*******************************************************************************
 * Module Macros
 *******************************************************************************/

/*******************************************************************************
 * Module Typedefs
 *******************************************************************************/
typedef enum { VFD_TYPE_1, VFD_TYPE_2 } VFD_TYPE;

// forward declaration
typedef struct VFD_DRIVER_STRUCT VFD_DRIVER;
typedef struct VFD_DRIVER_INTERFACE_STRUCT* VFD_DRIVER_INTERFACE;

typedef struct VFD_DRIVER_STRUCT {
  VFD_DRIVER_INTERFACE vtable;
  VFD_TYPE type;
} VFD_DRIVER_STRUCT;

typedef struct VFD_DRIVER_INTERFACE_STRUCT {
  uint8_t (*get_speed)(VFD_DRIVER*);
  void (*set_speed)(VFD_DRIVER*, uint8_t);
} VFD_DRIVER_INTERFACE_STRUCT;

/*******************************************************************************
 * Module Variable Definitions
 *******************************************************************************/

/*******************************************************************************
 *Function Prototypes
 *******************************************************************************/
uint8_t vfd_driver_get_speed(VFD_DRIVER* self);
void vfd_driver_set_speed(VFD_DRIVER* self, uint8_t speed);
VFD_TYPE vfd_driver_get_type(VFD_DRIVER* self);

#endif /* VFD_DRIVER_H_ */
